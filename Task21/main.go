package main

import (
	"fmt"
	"log"
)

func main() {
	du := NewDuck()
	du.Quack()
	log.Println("Duck Quack")

	adap := NewDuckAdapter(du)
	hu := NewHunter(&adap)
	hu.VoiceAsPrey()
	log.Println("Hunter voice as duck")
}

// Интерфейс, который должен быть у адаптера адаптера
type Voicer interface {
	Voice()
}

// Изначальный класс, который надо адаптировать
type Duck struct{}

func NewDuck() Duck {
	return Duck{}
}

func (du *Duck) Quack() {
	fmt.Println("Quack...")
}

// Адаптер это адаптер
type DuckAdapter struct {
	du Duck
}

func NewDuckAdapter(du Duck) DuckAdapter {
	return DuckAdapter{du: du}
}

func (adap *DuckAdapter) Voice() {
	adap.du.Quack()
}

// Hunter это клиент
type Hunter struct {
	prey Voicer
}

func NewHunter(prey Voicer) Hunter {
	return Hunter{prey: prey}
}

func (hu *Hunter) VoiceAsPrey() {
	hu.prey.Voice()
}
