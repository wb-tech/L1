package main

import (
	"fmt"
	"strconv"
	"sync"
)

func main() {

	// Можно использовать атомики для конкурентной работы с int счетчиком
	/*	count := atomic.Int64{}
		wgg := sync.WaitGroup{}

		for i := 0; i < 1000000; i++ {
			wgg.Add(1)
			go func(wg *sync.WaitGroup, cou *atomic.Int64) {
				defer wg.Done()
				cou.Add(1)
			}(&wgg, &count)
		}

		wgg.Wait()
		fmt.Println(count.Load())*/

	cou := NewConInt()
	wgg := sync.WaitGroup{}

	for i := 0; i < 999999; i++ {
		wgg.Add(1)
		go func(wg *sync.WaitGroup, count *ConInt) {
			defer wg.Done()
			count.Increment()
		}(&wgg, cou)
	}

	wgg.Wait()
	fmt.Println(cou)
}

type ConInt struct {
	mu   sync.Mutex
	data int
}

func NewConInt() *ConInt {
	return &ConInt{mu: sync.Mutex{}, data: 0}
}

func (con *ConInt) Increment() {
	con.mu.Lock()
	con.data++
	con.mu.Unlock()
}

func (con *ConInt) String() string {
	return strconv.Itoa(con.data)
}
