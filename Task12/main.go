package main

import (
	"L1/AlgsAndStruct"
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	fmt.Println("Enter strings separated comma ...")
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	text := scanner.Text()
	strs := strings.Split(text, ",")

	set := AlgsAndStruct.NewSet()

	for _, val := range strs {
		str := strings.Trim(val, " ")
		set.Insert(str)
	}

	fmt.Println(set)
}
