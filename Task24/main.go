package main

import (
	"fmt"
	"math"
)

func main() {
	x, y := 0.0, 0.0
	fmt.Println("Enter x and y for first point...")
	fmt.Scanln(&x, &y)
	po1 := NewPoint(x, y)

	fmt.Println("Enter x and y for second point...")
	fmt.Scanln(&x, &y)
	po2 := NewPoint(x, y)

	fmt.Println(po1.Calc(po2))
}

type Point struct {
	x, y float64
}

func NewPoint(x, y float64) Point {
	return Point{x, y}
}

func (po Point) GetX() float64 {
	return po.x
}

func (po Point) GetY() float64 {
	return po.y
}

func (po Point) Calc(po2 Point) float64 {
	delX := po.GetX() - po2.GetX()
	delY := po.GetY() - po2.GetY()

	return math.Sqrt(delX*delX + delY*delY)
}
