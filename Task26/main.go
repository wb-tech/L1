package main

import "fmt"

func main() {
	str := ""
	fmt.Println("Enter string...")
	fmt.Scan(&str)

	mp := make(map[rune]struct{})
	for _, k := range []rune(str) {
		if _, ok := mp[k]; ok {
			fmt.Println(false)
			return
		}
		mp[k] = struct{}{}
	}
	fmt.Println(true)
}
