package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	res := ""

	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("Enter string space...")
	scanner.Scan()
	text := []rune(scanner.Text())
	for i, _ := range text {
		res += string(text[len(text)-i-1])
	}

	fmt.Printf(res)
}
