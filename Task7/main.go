package main

import (
	"sync"
)

func main() {
	mp := NewMapConc()
	wg := sync.WaitGroup{}

	for i := 0; i < 1000; i++ {
		wg.Add(1)
		go func(k int) {
			defer wg.Done()
			mp.Set(k, k)
		}(i)
	}
	wg.Wait()
}

type MapConc struct {
	mu   *sync.Mutex
	dict map[int]int
}

func NewMapConc() *MapConc {
	return &MapConc{mu: &sync.Mutex{}, dict: make(map[int]int)}
}

func (mp MapConc) Set(key, val int) {
	mp.mu.Lock()
	mp.dict[key] = val
	mp.mu.Unlock()
}

func (mp MapConc) Get(key int) int {
	return mp.dict[key]
}
