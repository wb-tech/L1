package AlgsAndStruct

type Set struct {
	slice []string
}

func NewSet() *Set {
	return &Set{slice: make([]string, 0)}
}

func (se *Set) Len() int {
	return len(se.slice)
}

func (se *Set) Insert(val string) {
	if _, ok := se.Contains(val); ok {
		return
	}

	se.slice = append(se.slice, val)
}

func (set *Set) Intersect(set1 *Set) *Set {
	res := NewSet()
	for i := 0; i < set.Len(); i++ {
		if _, ok := set1.Contains(set.Get(i)); ok {
			res.Insert(set.Get(i))
		}
	}
	return res
}

func (se *Set) Contains(val string) (int, bool) {
	for i := range se.slice {
		if val == se.slice[i] {
			return i, true
		}
	}

	return -1, false
}

func (se *Set) Delete(val string) bool {
	ind, ok := se.Contains(val)
	if !ok {
		return false
	}

	se.slice = append(se.slice[:ind], se.slice[ind+1:cap(se.slice)]...)
	return true
}

func (se *Set) Get(ind int) string {
	if ind <= 0 && ind >= cap(se.slice) {
		return ""
	}

	return se.slice[ind]
}

func (se *Set) String() string {
	res := "( "
	for _, k := range se.slice {
		res += k + ", "
	}

	return res[:len(res)-2] + " )"
}
