package main

import (
	"context"
	"fmt"
	"math/rand"
	"time"
)

func main() {
	var tim int64 = 0
	fmt.Println("Enter deadline in seconds...")
	if _, err := fmt.Scan(&tim); err != nil {
		fmt.Println("Please enter count of seconds...")
		return
	}

	ch := make(chan int)
	dur := time.Duration(tim) * time.Second
	cont, cancel := context.WithTimeout(context.Background(), dur)
	defer cancel()

	go func(ctx context.Context, ch <-chan int) {
		val := 0
		for {
			select {
			case <-ctx.Done():
				fmt.Println("Reader stopped")
				return
			case val = <-ch:
				fmt.Println(val)
			}
		}
	}(cont, ch)

	gen(cont, ch)

	<-cont.Done()
}

func gen(ctx context.Context, ch chan<- int) {
	for {
		select {
		case <-ctx.Done():
			fmt.Println("Writer stopped")
			return
		default:
			ch <- rand.Intn(10000000)
		}
	}
}
