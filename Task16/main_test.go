package main

import (
	"fmt"
	"math/rand"
	"sort"
	"testing"
)

func Test(t *testing.T) {
	for i := 0; i < 1000; i++ {
		slice := make([]int, 10)
		for i := range slice {
			slice[i] = rand.Intn(10)
		}
		fmt.Println(slice)
		QuickSort(slice)
		fmt.Println(slice)
		fmt.Println(sort.IntsAreSorted(slice))
		fmt.Println()
	}

}
