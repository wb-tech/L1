package main

import (
	"fmt"
	"math/rand"
)

func main() {
	slice := make([]int, 100)
	for i := range slice {
		slice[i] = rand.Intn(10)
	}

	fmt.Printf("Unsorted slice: %v\n", slice)

	QuickSort(slice)

	fmt.Printf("Sorted slice: %v\n", slice)
}

func QuickSort(slice []int) {
	if len(slice) > 1 {
		ind, kv := partition(slice)
		if kv <= len(slice) {
			QuickSort(slice[:kv])
		}
		if ind < len(slice) {
			QuickSort(slice[ind:])
		}
	}
}

func partition(slice []int) (int, int) {
	pivot := slice[len(slice)-1] //rand.Intn(len(slice))]

	ind, kv := 0, 0

	for i := 0; i < len(slice); i++ {
		if slice[i] < pivot {
			if kv == ind {
				slice[i], slice[kv] = slice[kv], slice[i]
			} else {
				slice[i], slice[kv] = slice[kv], slice[i]
				slice[i], slice[ind] = slice[ind], slice[i]
			}
			ind++
			kv++
		} else if pivot == slice[i] {
			slice[i], slice[ind] = slice[ind], slice[i]
			ind++
		}
	}
	return ind, kv
}

//func partition(slice []int) int
