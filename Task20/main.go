package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

// TODO: Вариант со сором строки
func main() {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("Enter words separated space...")
	scanner.Scan()
	words := strings.Split(scanner.Text(), " ")

	// Способ со сбором новой строки в обратном порядке
	/*	build := strings.Builder{}
		for i := len(words) - 1; i >= 0; i-- {
			build.WriteString(words[i] + " ")
		}
		fmt.Println(build.String())*/

	// Способ с обменом слов в массиве на противоположный
	for i := 0; i < len(words)/2; i++ {
		words[i], words[len(words)-1-i] = words[len(words)-1-i], words[i]
	}

	fmt.Println(strings.Join(words, " "))
}
