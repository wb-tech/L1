package main

import (
	"fmt"
	"log"
	"strconv"
)

func main() {
	hu := NewHuman("Albert", 23)
	ac := NewAction(hu)

	// Запуск метода из структуры Human
	log.Println("Start run from Human...")
	hu.Run()

	// Запуск метода из структуры Action, который запускает метод из Human и этот же метод, но ввиде встраивания
	log.Println("Start run from Action as Human in Action and Action")
	ac.StartRun()

	//Запуск встраеваемого метода из Human в структуре Action
	log.Println("Start run from Action as Action Human")
	ac.Run()
}

type Human struct {
	Name string
	Age  int
}

func NewHuman(name string, age int) *Human {
	return &Human{Name: name, Age: age}
}

func (hu Human) Run() {
	fmt.Println("Human " + hu.Name + " run at " + strconv.Itoa(hu.Age) + " age...")
}

type Action struct {
	*Human
}

func NewAction(hu *Human) *Action {
	return &Action{Human: hu}
}

func (ac Action) StartRun() {
	ac.Human.Run()
	ac.Run()
}
