package main

import (
	"fmt"
	"math/big"
)

func main() {
	a, b := &big.Int{}, &big.Int{}

	a.SetString("1000000000000000000000000000000000000000000", 2)
	b.SetString("1000000010000000000000000000000000000000000", 2)

	//Sum
	fmt.Println(a.Add(a, b).String())

	//Sub
	fmt.Println(a.Sub(a, b).String())

	//Mul
	fmt.Println(a.Mul(a, b).String())

	//Div
	fmt.Println(a.Div(a, b).String())
}
