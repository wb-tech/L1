package main

import (
	"context"
	"fmt"
	"math/rand"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

var n int

// TODO: Сделать читаемо
func main() {
	fmt.Println("Enter count of goroutines...")
	fmt.Scan(&n)

	// Создаем канал для отслеживания Ctrl+C и подписываемся на уведомление
	in := make(chan os.Signal, 1)
	signal.Notify(in, syscall.SIGINT)

	//Создаем wg и конекст для корректного завершения программы
	ctx, cancel := context.WithCancel(context.Background())
	wg := sync.WaitGroup{}
	out := make(chan int)

	for i := 0; i < n; i++ {
		wg.Add(1)
		go read(ctx, &wg, out, i)
	}

	gen(&cancel, out, in)

	//Ожидаем пока не будет закончены все горутины и завершена программа Ctrl+C
	wg.Wait()
	fmt.Println("Go workers stopped...")
}

// Генерируем числа до тех пор, пока не получим уведомление о остановке с Ctrl+C
func gen(cancel *context.CancelFunc, out chan<- int, in <-chan os.Signal) {
	for {
		val := rand.Intn(10000000)
		select {
		case <-in:
			(*cancel)()
			return
		default:
			out <- val
		}
	}
}

// Читаем канал со сгенерированными числами до тех пор, пока не произойдет отмена контекста
func read(ctx context.Context, wg *sync.WaitGroup, out chan int, k int) {
	var val int
	for {
		select {
		case <-ctx.Done():
			wg.Done()
			fmt.Printf("Gorutine %v stop\n", k)
			return
		case val = <-out:
			fmt.Println(val)
		}
	}
}
