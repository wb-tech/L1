package main

import (
	"fmt"
	"strings"
)

var justString string

func main() {
	someFunc()
	fmt.Println(justString)
}

func someFunc() {
	v := createHugeString(1 << 10)
	justString = string(append([]byte{}, v[:100]...))
}

// Проблема данной функции в том, что при присваивании значения justString таким образом,
// в памяти также остается выделенная память на изначальную не укороченную строку, которую GC не может очистить,
// так как justString все еще хранит ссылку на часть памяти изначальной строки.
/*func someFunc() {
	v := createHugeString(1 << 10)
	justString = v[:100]
}*/

func createHugeString(n int) string {
	build := strings.Builder{}
	for i := 0; i < n; i++ {
		build.WriteString(string('A' + i%(25*2)))
	}
	return build.String()
}
