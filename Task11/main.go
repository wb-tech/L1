package main

import (
	"L1/AlgsAndStruct"
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	set1 := AlgsAndStruct.NewSet()
	set2 := AlgsAndStruct.NewSet()
	scanner := bufio.NewScanner(os.Stdin)

	fmt.Println("Enter first set values separated comma ...")
	scanner.Scan()
	text := scanner.Text()
	strs1 := strings.Split(text, ",")

	for _, val := range strs1 {
		str := strings.Trim(val, " ")
		set1.Insert(str)
	}

	fmt.Println("Enter first set values separated comma ...")
	scanner.Scan()
	text = scanner.Text()
	strs2 := strings.Split(text, ",")

	for _, val := range strs2 {
		str := strings.Trim(val, " ")
		set2.Insert(str)
	}

	fmt.Println(set1.Intersect(set2))
}
