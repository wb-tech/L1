package main

import (
	"context"
	"fmt"
	"time"
)

func main() {
	/*
		Способ полной остановки горутины с использованием канала
	*/
	ch := make(chan int)

	go func() {
		fmt.Println("New goroutine 1")
		<-ch
		fmt.Println("Stop goroutine 1")
	}()

	time.Sleep(time.Second * 2)
	close(ch)

	/*
		Способ полной остановки горутины с использованием контекста
	*/
	cont, can := context.WithCancel(context.Background())

	go func() {
		fmt.Println("New goroutine 2")
		<-cont.Done()
		fmt.Println("Stop goroutine 2")
	}()

	time.Sleep(time.Second * 2)
	can()

	/*
		Способ частичной остановки горутины с использованием канала и цикла с селектом.
	*/
	newCh := make(chan int)

	go func() {
		fmt.Println("New goroutine 3")
		for {
			select {
			case <-newCh:
				fmt.Println("Stop gorutine 3")
				return
			default:
				fmt.Println("goroutine 3 work")
			}
		}
	}()

	time.Sleep(time.Second * 2)
	close(newCh)

}
