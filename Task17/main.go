package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	fmt.Println("Enter slice value separated comma...")
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	text := scanner.Text()

	strs := strings.Split(text, ",")
	slice := make([]int, len(strs))

	for i, _ := range strs {
		str := strings.Trim(strs[i], " ")
		val, err := strconv.Atoi(str)
		if err != nil {
			log.Println(err)
			return
		}
		slice[i] = val
	}

	fmt.Println("Enter value for search in slice ...")

	scanner.Scan()
	text = scanner.Text()

	val, err := strconv.Atoi(text)
	if err != nil {
		log.Println(err)
		return
	}

	fmt.Println(BinarySearchLoop(slice, val))
}

func BinarySearchLoop(slice []int, val int) int {
	left := 0
	right := cap(slice) - 1
	mid := (right - left) / 2
	for left <= right {
		if val < slice[mid] {
			right = mid - 1
			mid = right - (right-left)/2
		} else if val > slice[mid] {
			left = mid + 1
			mid = left + (right-left)/2
		} else {
			return mid
		}
	}
	return -1
}
