package main

import (
	"fmt"
	"math/rand"
)

func main() {
	n := 0
	fmt.Println("Enter length slice")
	if _, err := fmt.Scan(&n); err != nil {
		fmt.Println("Please... enter length slice")
		return
	}
	if n <= 0 {
		fmt.Println("Slice length must be more then 0 ")
		return
	}

	arr := make([]int, n)
	for i := range arr {
		arr[i] = rand.Intn(10000)
	}

	fmt.Println(arr)

	ind := 0
	fmt.Println("Enter index for delete...")
	if _, err := fmt.Scan(&ind); err != nil {
		fmt.Println("Please... enter index of slice")
		return
	}

	if ind < 0 || ind >= cap(arr) {
		fmt.Println("Index out of range")
		return
	}

	fmt.Println(append(arr[:ind], arr[ind+1:]...))
}
