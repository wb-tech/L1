package main

import (
	"fmt"
	"time"
)

func main() {
	Sleep(time.Second * 5)
	fmt.Println("Sleep finish")
}

func Sleep(duration time.Duration) {
	next := time.Now().Add(duration)
	for time.Now().Before(next) {

	}
}
