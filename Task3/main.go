package main

import (
	"fmt"
	"sync"
)

func main() {
	sum := 0
	wg := sync.WaitGroup{}

	//Использую waitGroup для последовательного добавления суммы к квадрату
	/*
		for i := 2; i <= 10; i += 2 {
			wg.Add(1)
			go func(k int) {
				sum += k * k
				wg.Done()
			}(i)
			wg.Wait()
		}
	*/

	// Используем мьютекс для блокировки переменной sum
	mu := sync.Mutex{}

	for i := 2; i <= 10; i += 2 {
		wg.Add(1)
		go func(k int) {
			defer wg.Done()
			mu.Lock()
			sum += k * k
			mu.Unlock()
		}(i)
	}

	wg.Wait()
	fmt.Println(sum)
}
