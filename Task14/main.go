package main

import (
	"fmt"
	"reflect"
)

func main() {
	a := 0
	fmt.Println(getType(a))
	b := "sadf"
	fmt.Println(getType(b))
	c := true
	fmt.Println(getType(c))
	d := make(chan int)
	fmt.Println(getType(d))
	f := 1.1
	fmt.Println(getType(f))
}

func getType(val interface{}) string {
	switch reflect.TypeOf(val).Kind() {
	case reflect.Int:
		return "int"
	case reflect.String:
		return "string"
	case reflect.Bool:
		return "bool"
	case reflect.Chan:
		return "chan"
	default:
		return "unknown"
	}
}
