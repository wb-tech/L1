package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	//Инициализируем мапу для слайсов
	res := make(map[int][]float64)

	//Читаем с ввода температуру
	fmt.Println("Enter temperature separated comma...")
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	tempStr := scanner.Text()

	//Температуры разбиваем через запятую
	temps := strings.Split(tempStr, ",")

	//Обходим полученные температуры
	for _, k := range temps {

		//Получаем конкретную температуру
		temp, err := strconv.ParseFloat(strings.Trim(k, " "), 64)
		if err != nil {
			log.Println(err)
			return
		}

		//Считаем ключ для температуры
		key := int(temp) / 10 * 10

		//Если ключа нет в мапе, то инициализируем слайс для ключа в мапе
		if _, ok := res[key]; !ok {
			res[key] = make([]float64, 0)
		}

		//Добавляем температуру к слайсу по ключу
		res[key] = append(res[key], temp)
	}

	fmt.Println(res)
}
