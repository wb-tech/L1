package main

import (
	"fmt"
	"log"
)

func main() {
	var val, ind, set int64
	fmt.Println("Enter number...")
	if _, err := fmt.Scan(&val); err != nil {
		fmt.Println("Please... Enter a number...")
	}

	fmt.Println("Enter num bit of number")
	if _, err := fmt.Scan(&ind); err != nil {
		fmt.Println("Please... Enter a number of bit...")
	}
	ind--

	fmt.Println("Enter value of bit")
	if _, err := fmt.Scan(&set); err != nil || (set < 0 && set > 1) {
		fmt.Println("Please... Enter a value of bit...")
	}
	if set != 0 && set != 1 {
		log.Println("Value for bit change is not 1 and not 0")
	}

	fmt.Printf("%b\n", val)
	if set == 1 {
		res := val | (1 << ind)
		fmt.Printf("%b\n", res)
	} else {
		res := val & ^(1 << ind)
		fmt.Printf("%b\n", res)
	}
}
