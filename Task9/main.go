package main

import (
	"fmt"
	"sync"
)

func main() {
	base := make(chan int)
	sqr := make(chan int)
	slice := make([]int, 0)

	for i := 0; i < 10000; i++ {
		slice = append(slice, i)
	}

	wg := sync.WaitGroup{}

	wg.Add(3)
	go InitChan(&wg, slice, base)
	go SqrChan(&wg, base, sqr)
	go ReaderChan(&wg, sqr)

	wg.Wait()
}

func InitChan(wg *sync.WaitGroup, slice []int, out chan<- int) {
	defer wg.Done()
	for _, item := range slice {
		out <- item
	}
	close(out)
}

func SqrChan(wg *sync.WaitGroup, in <-chan int, out chan<- int) {
	defer wg.Done()
	for item := range in {
		out <- item * item
	}
	close(out)
}

func ReaderChan(wg *sync.WaitGroup, in <-chan int) {
	defer wg.Done()
	for item := range in {
		fmt.Println(item)
	}
}
